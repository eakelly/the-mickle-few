
import unittest

import tile as ti
import anim as a

class AnimationTests(unittest.TestCase):

	def test_tile_animation(self):
		tile = ti.Tile(None, 5, 10)
		t = a.TileAnimation(tile, [24, 0, 29], 1)
		
		self.assertEqual(t.display(1), 24)
		self.assertEqual(t.display(1), 24)
		
		self.assertEqual(t.display(2), 0)
		
		self.assertEqual(t.display(5), 29)
		
		self.assertIsNone(t.display(6))
		
	def test_looping_animation(self):
		tile1 = ti.Tile(None, 1, 2)
		tile2 = ti.Tile(None, 11, 22)
		l = a.LoopingAnimation([1,2], [11,22], tile2, None, [5,6,7], 6, 1)
		
		self.assertEqual(l.display(1), (5, 1, 2))
		self.assertEqual(l.display(1), (5, 1, 2))
		
		self.assertEqual(l.display(2), (6, 3, 6))

		self.assertEqual(l.display(4), (7, 5, 10))

		self.assertEqual(l.display(7), (5, 7, 14))
		
		self.assertEqual(l.display(10), (6, 9, 18))
		
		self.assertEqual(l.display(20), (7, 11, 22))
		
		self.assertIsNone(l.display(22)[0])
		
if __name__ == '__main__':
	unittest.main()