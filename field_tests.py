import field as f
import const as c

import unittest

class FieldsTests(unittest.TestCase):

	def test_rectangle_board(self):
		field = f.Field(2, 3)
		
		self.assertTrue(field.tile_exists(0, 0))
		self.assertTrue(field.tile_exists(1, 2))
		
		self.assertFalse(field.tile_exists(-1, -1))
		self.assertFalse(field.tile_exists(2, 2))
		self.assertFalse(field.tile_exists(3, 3))

	def test_square_board(self):
		field = f.Field(2)
		
		self.assertTrue(field.tile_exists(1, 1))
		self.assertFalse(field.tile_exists(2, 2))

	def test_empty_board(self):
		field = f.Field()
		
		self.assertFalse(field.tile_exists(0, 0))

	def test_can_travel1(self):
		field = f.Field(2)
		
		self.assertTrue(field.can_travel(0, 0, c.NORTH))
		self.assertTrue(field.can_travel(0, 0, c.EAST))
		self.assertTrue(field.can_travel(1, 1, c.SOUTH))
		self.assertTrue(field.can_travel(1, 1, c.WEST))
		
	def test_can_travel2(self):
		field = f.Field(2)
		
		self.assertFalse(field.can_travel(1, 1, c.NORTH))
		self.assertFalse(field.can_travel(1, 1, c.EAST))
		self.assertFalse(field.can_travel(0, 0, c.SOUTH))
		self.assertFalse(field.can_travel(0, 0, c.WEST))
		
		self.assertFalse(field.can_travel(-1, -1, c.WEST))
		
	def test_get_tile1(self):
		field = f.Field(1, 1)
		
		t = field.get_tile(0, 0)
		self.assertTrue(t is not None)
		
	def test_get_tile2(self):
		field = f.Field(1, 1)
		
		t = field.get_tile(0, -1, c.NORTH)
		self.assertTrue(t is not None)

	def test_add_tile1(self):
		field = f.Field()
		
		self.assertFalse(field.tile_exists(0, 0))
		field.add_tile(0, 0)
		self.assertTrue(field.tile_exists(0, 0))
		
	def test_add_tile2(self):
		field = f.Field()
		
		self.assertFalse(field.tile_exists(0, 0))
		field.add_tile(0, -1, c.NORTH)
		self.assertTrue(field.tile_exists(0, 0))
		
	def test_remove_tile1(self):
		field = f.Field(1, 1)
		
		self.assertTrue(field.tile_exists(0, 0))
		field.remove_tile(0, 0)
		self.assertFalse(field.tile_exists(0, 0))
		
	def test_remove_tile2(self):
		field = f.Field(1, 1)
		
		self.assertTrue(field.tile_exists(0, 0))
		field.remove_tile(0, -1, c.NORTH)
		self.assertFalse(field.tile_exists(0, 0))
		
	def test_add_wall(self):
		field = f.Field(2, 1)
		
		self.assertTrue(field.can_travel(0, 0, c.EAST))
		self.assertTrue(field.can_travel(1, 0, c.WEST))
		
		field.add_wall(0, 0, c.EAST)
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
		
	def test_remove_wall(self):
		field = f.Field(2, 1)
		field.add_wall(0, 0, c.EAST)
		
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
		
		field.remove_wall(0, 0, c.EAST)
		self.assertTrue(field.can_travel(0, 0, c.EAST))
		self.assertTrue(field.can_travel(1, 0, c.WEST))
		
	def test_add_wall_to(self):
		field = f.Field(2, 1)
		
		field.add_wall_to(0, 0, c.EAST)
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertTrue(field.can_travel(1, 0, c.WEST))
		
		field.add_wall_to(1, 0, c.WEST)
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
		
		
	def test_add_wall_from(self):
		field = f.Field(2, 1)
		
		field.add_wall_from(0, 0, c.EAST)
		self.assertTrue(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
		
		field.add_wall_from(1, 0, c.WEST)
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
	
	def test_remove_wall_to(self):
		field = f.Field(2, 1)
		
		field.add_wall(0, 0, c.EAST)
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
		
		field.remove_wall_to(0, 0, c.EAST)
		self.assertTrue(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
		
		field.remove_wall_to(1, 0, c.WEST)
		self.assertTrue(field.can_travel(0, 0, c.EAST))
		self.assertTrue(field.can_travel(1, 0, c.WEST))
	
	def test_remove_wall_from(self):
		field = f.Field(2, 1)
		field.add_wall(0, 0, c.EAST)
		
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertFalse(field.can_travel(1, 0, c.WEST))
		
		field.remove_wall_from(0, 0, c.EAST)
		self.assertFalse(field.can_travel(0, 0, c.EAST))
		self.assertTrue(field.can_travel(1, 0, c.WEST))
		
		field.remove_wall_from(1, 0, c.WEST)
		self.assertTrue(field.can_travel(0, 0, c.EAST))
		self.assertTrue(field.can_travel(1, 0, c.WEST))
		
if __name__ == '__main__':
	unittest.main()
