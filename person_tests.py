import person as p
import field as f
import const as c

import unittest

class PersonWalk(unittest.TestCase):
	def runTest(self):
		field = f.Field(5,5) #field.add_person(5, 5, p.Archer(0))
		a = p.Person(0)
		field.add_person(0, 0, a)
		a.movement = a.available_movement = 2
		
		self.assertTrue(a.move(field.tiles[0,1]))
		a.available_movement = 2
		self.assertTrue(a.move(field.tiles[0,3]))
		self.assertFalse(a.move(field.tiles[2,4]))
		
class DeadPersonAttacks(unittest.TestCase):
	def runTest(self):
		field = f.Field(5)
		a = p.Warrior(0)
		a.hp = 0
		b = p.Warrior(1)
		field.add_person(0, 0, a)
		field.add_person(1, 0, b)
		
		self.assertFalse(a.attack(b))
		self.assertFalse(b.attack(a))
		
class PersonWalkThroughWall(unittest.TestCase):
	def runTest(self):
		field = f.Field(5,5)
		field.tiles[0,0].add_wall(c.EAST)
		a = p.Person(0)
		a.movement = 2
		field.add_person(0, 0, a)
		
		self.assertFalse(a.move(field.tiles[1,0]))
		
class PersonAttack(unittest.TestCase):
	def runTest(self):
		field = f.Field(5, 5)
		a = p.Person(0)
		b = p.Warrior(1)
		field.add_person(1, 2, a)
		field.add_person(2, 2, b)
		
		a.attack(b)
		self.assertEqual(b.hp, 5)
		
		a.attack(b)
		self.assertEqual(b.hp, 0)
		self.assertFalse(b.is_alive())

class HealerHeal(unittest.TestCase):
	def runTest(self):
		field = f.Field(6)
		a = p.Healer(1)
		b = p.Warrior(0)
		field.add_person(5, 5, a)
		field.add_person(5, 4, b)
		
		self.assertTrue(a.attack(b))
		self.assertEqual(b.hp, 10)
		
		a.attack(b)
		self.assertEqual(b.hp, 10)
		
class ArcherAttack(unittest.TestCase):
	def runTest(self):
		field = f.Field(4)
		a = p.Archer(1)
		b = p.Person(0)
		field.add_person(0, 0, a)
		field.add_person(1, 1, b)
		
		self.assertTrue(a.attack(b))
		
		field.add_person(1, 0, b)
		self.assertFalse(a.attack(b))
		
		field.add_person(2, 0, b)
		self.assertTrue(a.attack(b))
		
		field.add_person(3, 0, b)
		self.assertTrue(a.attack(b))
		
		field.add_person(3, 1, b)
		self.assertFalse(a.attack(b))
	
if __name__ == '__main__':
	unittest.main()
