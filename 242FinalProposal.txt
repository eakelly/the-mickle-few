﻿THE MICKLE FEW
Elizabeth Kelly and Blake Petermeier
eakelly3 petermr2




1. Abstract
1.1 Purpose of this Document
	This document describes the requirements and features of The Mickle Few, a tile-based strategy game.  It also includes a mockup of the game and a timeline for implementation of the features.
1.2 Background and Motivation
	Production of The Mickle Few will provide experience for working in the game industry in the future.  It will help in understanding the game development process and the team dynamics involved.


2. Technical Specifications
2.1 Platform: Windows/Linux
2.2 Language: Python
2.3 Coding Standard: PEP 8 except with tabs
2.4 Libraries: pygame
2.5 IDE: Notepad++ and Eclipse/PyDev
2.6 Interface: Native App


3 Functional Specifications
3.1 Affordances
* Intuitive, easy-to-use interface.
* Game data persists after closing program (save and load game states).
* Users take turns on a per-character basis.
* Game environment allows terrain to block character movement.
* Character combat scales relative to enemies and equipment.
* Game events are animated.
* Single player can play against the computer.
3.2 Features
* Interface interaction will be handled with pygame mouse events.
* Save/load games with a simple file format.
* Individual character stats will be used to derive important gameplay attributes.
* Character turn priority will be determined using a calculated speed value.
* Combat will take into account both characters’ stats and equipment.
* Terrain traversal will be controlled using directed graph edges.
* Events can display animations while pausing the main game loop.
* AI will use pathfinding and decision making algorithms to play against humans.






3.3 Mockup



Timeline
Week 1 : Primitive Game Logic
	Blake: Board representation and movement
	Elizabeth: Attack functions and piece statistics


Week 2 : Game Loop and Visuals
	Blake: Alternating player turns affecting game state; environment art
	Elizabeth: Displaying map, walls, and (static) pieces; character art


Week 3 : Animation and State Saving/Loading
	Blake: File format for storing character/map data
	Elizabeth: Animation for movement and attacking


Week 4 : AI (Automated enemy movement and attacking) and Polish (Gameplay enhancement)
	Blake: Collection and use of equipment to change character stats
	Elizabeth: Enemy pathfinding and decision making


Possible Future Enhancements
* In-Game Map Editor
* Network Play