
class Calculator(object):

	def __init__(self, field):
		""" Creates the Calculator, which will store the order of turns for all
		Persons on the given Field. """
		self.field = field
		self.persons = field.get_all_persons()
		self.max_speed = calculate_max_speed(self.persons)
		
		# array where the index represents how many movement points must pass
		# before the person at that index can move again.
		# remove the first person in the array at index zero.  they may now
		# move.
		# put them at the end of the array at index 'max_speed - speed + 2' of
		# the outer array.
		# while the array at index zero is empty, remove the first element of
		# the outer array.
		self.turns = self.create_turn_queue(self.persons)
		
	def create_turn_queue(self, persons):
		""" Creates the list of lists representing when each Person should take
		its next turn. """
		queue = [[] for x in xrange(self.max_speed + 2)]
		for person in persons:
			index = calculate_index(self.max_speed, person.speed)
			queue[index].append(person)
		return queue
		
	def get_next_turn(self, current_animation):
		""" Returns the next Person to take a turn, and updates the queue. """
		if current_animation == None:
			if len(self.persons) != (len(self.field.get_all_persons())):
				print len(self.persons), len(self.field.get_all_persons()),
				print "new queue"
				self.persons = self.field.get_all_persons()
				self.turns = self.create_turn_queue(self.persons)
		while not self.turns[0]: # infinite loop if everyone is dead
			self.turns.pop(0)
			self.turns.append([])
		next_person = self.turns[0].pop(0)
		if not next_person.is_alive():
			return self.get_next_turn(current_animation)
		next_person.available_movement = next_person.movement
		new_index = calculate_index(self.max_speed, next_person.speed)
		self.turns[new_index].append(next_person)
		print next_person, 'is to act'
		return next_person
		
		
def calculate_index(max_speed, speed):
	""" Calculates the index to be used to place Persons at the end of the
	queue. """
	return max_speed - speed + 2
		
def calculate_max_speed(persons):
	""" Returns the maximum speed stat of all Persons. """
	max = 0
	for person in persons:
		if person.speed > max:
			max = person.speed
	return max
	
