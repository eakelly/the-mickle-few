import pygame
import sys
import datetime

import person as p
import image as i
import field as f
import team as t
import anim as a
import view_mode as vm
import const

class View(object):

	"""Handles all images, and sends clicks to the Controller."""

	def __init__(self, field, field_range):
		"""Initialize the View object, load all images, and start up pygame."""

		self.field = field
		self.FIELD_RANGE = field_range
		
		self.set_display(40, 5, 1)
		
		pygame.init()
		pygame.display.set_caption("The Mickle Few")
		self.screen = pygame.display.set_mode(self.SCREEN_SIZE)
		
		self.background = pygame.Color("darkolivegreen")
		self.image_loader = i.ImageLoader(self.TILE_SIZE, self.WALL_SIZE)
		
		self.tile_rects = []
		
		self.animation = None
		self.active_rect = None
		self.target_rect = None
		
		self.turn_ended = False
		self.view_mode = vm.ViewMode(self)
		
	def set_display(self, general_scale, tile_scale, wall_scale):
		""" Sets up the screen so that everything fits with the given scales.
		
		Arguments:
		general-scale -- A scaling factor for everything
		tile-scale -- A scaling factor for the size of Tiles
		wall-scale -- A scaling factor for the width of walls between Tiles
		"""
		self.TILE_SIZE = int(1.0 * general_scale * tile_scale / (wall_scale + tile_scale))
		self.WALL_SIZE = int(1.0 * general_scale * wall_scale / (wall_scale + tile_scale))
		self.TOTAL_SIZE = self.WALL_SIZE + self.TILE_SIZE
		self.FIELD_ANCHOR = 0, 0
		self.FIELD_SIZE = ((self.FIELD_RANGE[0] * self.TOTAL_SIZE), 
		                   (self.FIELD_RANGE[1] * self.TOTAL_SIZE))
		self.EXTRA_SIZE = 100, 0
		self.SCREEN_SIZE = ((self.FIELD_SIZE[0] + self.EXTRA_SIZE[0]),
		                    (self.FIELD_SIZE[1] + self.EXTRA_SIZE[1]))
		
	def get_mouse_rect(self):
		""" Returns the Rectangle that the mouse is currently pointing at. """
		for rect in self.tile_rects:
			if rect.collidepoint(pygame.mouse.get_pos()):
				return rect
		return None
		
	def get_tile_anchor(self, x, y):
		""" Calculates top left screen position of a displayed Tile at (x,y).
		"""
		return (self.TOTAL_SIZE * x + self.WALL_SIZE,
			    self.TOTAL_SIZE * y + self.WALL_SIZE)

	def get_tile_coords(self, x, y):
		""" Calculates Tile coordinates from given screen position. """
		return ((x - self.WALL_SIZE) / self.TOTAL_SIZE,
			    (y - self.WALL_SIZE) / self.TOTAL_SIZE)
		
	def get_wall_anchor(self, x, y, direction):
		""" Returns the edge of the Tile at (x,y) in the given direction. """
		if direction == const.NORTH: # bottom
			return self.get_wall_anchor(x, y + 1, const.get_opposite_direction(direction))
		elif direction == const.EAST: # right
			return self.get_wall_anchor(x + 1, y, const.get_opposite_direction(direction))
		elif direction == const.SOUTH: # top
			return (self.TOTAL_SIZE * x,
			        self.TOTAL_SIZE * y)
		elif direction == const.WEST: # left
			return (self.TOTAL_SIZE * x,
			        self.TOTAL_SIZE * y)
	
	def display_wall(self, x, y, direction):
		""" Displays a wall on the 'direction' side of the Tile at (x,y). """
		x_loc, y_loc = self.get_wall_anchor(x, y, direction)
		if direction == const.NORTH or direction == const.SOUTH:
			horiz_wall_rect = pygame.Rect(x_loc,
										  y_loc,
										  self.TILE_SIZE + 2 * self.WALL_SIZE,
										  self.WALL_SIZE)
			horiz_wall_img = self.image_loader.horiz_wall
			self.screen.blit(horiz_wall_img, horiz_wall_rect)
		elif direction == const.EAST or direction == const.WEST:
			vert_wall_rect = pygame.Rect(x_loc,
										 y_loc,
										 self.WALL_SIZE,
										 self.TILE_SIZE + 2 * self.WALL_SIZE)
			vert_wall_img = self.image_loader.vert_wall
			self.screen.blit(vert_wall_img, vert_wall_rect)
			
	def display_walls(self):
		""" Displays all walls that exist in the Field. """
		for tile in self.field.tiles.values():
			for direction in const.directions:
				if tile.walls[direction]:
					self.display_wall(tile.x, tile.y, direction)
			
		
	def get_tile(self, rect):
		""" Calculates the tile based on the rect given. """
		x, y = self.get_tile_coords(rect.left, rect.top)
		return self.field.get_tile(x, y)
		
	def associate_rects(self):
		""" Creates Rectangle lists for all Tiles and Persons, as well as
		empty Tiles and the overall Field. """
		self.field_rect = pygame.Rect(0, 0, self.FIELD_SIZE[0], self.FIELD_SIZE[1])
		self.rects = []
		self.tile_rects = []
		self.blank_rects = []
		self.field_rects = []
		self.person_rects = []
		for x in range(self.FIELD_RANGE[0]): # only display tiles in view
			for y in range(self.FIELD_RANGE[1]):
				x_loc, y_loc = self.get_tile_anchor(x, y)
				current_rect = pygame.Rect(x_loc, y_loc, self.TILE_SIZE, self.TILE_SIZE)
				self.tile_rects.append(current_rect)
				if self.field.tile_exists(x, y):
					self.field_rects.append(current_rect)
					if self.field.has_person(x, y):
						self.person_rects.append(current_rect)
				else:
					self.blank_rects.append(current_rect)
		
	def display_tiles(self):
		"""Displays all tiles on board."""
		for field_rect in self.field_rects:
			self.display_field_rect(field_rect)
		for blank_rect in self.blank_rects:
			pass
			#self.display_blank_rect(blank_rect)
				
	def display_field_rect(self, field_rect):
		""" Displays grass over the entire Field. """
		tile_img = self.image_loader.grass
		self.screen.blit(tile_img, field_rect, self.field_rect)
	
	def display_blank_rect(self, blank_rect):
		""" Displays an empty Rectangle, to be used for missing Tiles. """
		tile_img = self.image_loader.blank
		self.screen.blit(tile_img, blank_rect)
		
	def display_persons(self):
		""" Displays all Persons in their relative positions on the screen. """
		for person_rect in self.person_rects:
			tile = self.get_tile(person_rect)
			person = tile.person
			person_img = self.image_loader.get_image(person)
			self.screen.blit(person_img, person_rect)
			
	def display_hp_bars(self):
		""" Displays a hp bar for each Person.  More green means a greater
		percentage of hp remaining. """
		for person_rect in self.person_rects:
			tile = self.get_tile(person_rect)
			person = tile.person
			hp_percent = float(person.hp) / person.max_hp
			hp_rect = pygame.Rect(person_rect[0], 
			                      person_rect[1] - self.TILE_SIZE/10, 
			                      self.TILE_SIZE, 
			                      self.TILE_SIZE/5)
			green_img = self.image_loader.green_bar
			red_img = self.image_loader.red_bar
			self.screen.blit(red_img, hp_rect)
			self.screen.blit(green_img, hp_rect, pygame.Rect(0, 0, self.TILE_SIZE * hp_percent, self.TILE_SIZE/5))
		
	def display_active_selection(self):
		"""Displays a marker for the Person whose turn it is."""
		img = self.image_loader.get_next_turn()
		if self.active_rect is not None:
			x_loc, y_loc = self.active_rect[0], self.active_rect[1]
			self.screen.blit(img, pygame.Rect(x_loc, y_loc,
											  self.TILE_SIZE,
											  self.TILE_SIZE))
									     
	def display_target_selection(self):
		"""Displays a marker for the Person whose turn it is."""
		img = self.image_loader.get_next_turn()
		if self.target_rect is not None:
			x_loc, y_loc = self.target_rect[0], self.target_rect[1]
			self.screen.blit(img, pygame.Rect(x_loc, y_loc,
											  self.TILE_SIZE,
											  self.TILE_SIZE))
								
	def action(self, event):
		""" Finds the action to take based on the past event. """
		self.view_mode.action(event)
		
	def check_for_click(self):
		"""Check for clicks."""
		self.turn_ended = False
		if self.animation != None:
			return
		event_list = pygame.event.get()
		for event in event_list:
			self.action(event)
					
	def display(self):
		"""Redisplay everything if a click has occurred."""
		self.screen.fill(self.background)
		self.associate_rects()
		self.display_tiles()
		self.display_walls()
		self.display_persons()
		self.display_hp_bars()
		self.display_active_selection()
		self.display_animation()
		pygame.display.flip()
		#pygame.display.update(list_of_dirty_rects) #speed up rendering if need be
					
	def display_animation(self):
		""" Displays the animation, if one is occurring. """
		if not self.animation:
			return
		if isinstance(self.animation, a.LoopingAnimation): # lol not pythonic
			image, x, y = self.animation.display(pygame.time.get_ticks())
			if not image:
				self.animation = None
			else:
				self.screen.blit(image, (x, y, self.TILE_SIZE, self.TILE_SIZE))
		else: # a.TileAnimation
			image = self.animation.display(pygame.time.get_ticks())
			if not image:
				self.animation = None
			else:
				tile = self.animation.tile
				coords = self.get_tile_anchor(tile.x, tile.y)
				self.screen.blit(image, (coords[0], coords[1], self.TILE_SIZE, self.TILE_SIZE))
			
	def display_game_over(self, loser):
		"""Displays the 'game over' screen with the name of the team that won.
		"""
		img = self.image_loader.purple_wins
		if loser == 0:
			img = self.image_loader.teal_wins
		self.screen.blit(img, pygame.Rect(	(self.SCREEN_SIZE[0]-500)/2,
											(self.SCREEN_SIZE[1]-500)/2,
											500,
											500))
		pygame.display.flip()
		while(1):
			for event in pygame.event.get():
				if event.type == pygame.QUIT or event.type == pygame.MOUSEBUTTONUP:
					sys.exit()
