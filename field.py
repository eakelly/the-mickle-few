from tile import Tile
import person as p
import random
import ai as a

import const as util

class Field(object):

	def __init__(self, x0 = None, y0 = None):
		"""Create a field. Blank if no dimensions specified. Square if one specified. Rectangular if both specified.
	
		Arguments:
		x0 -- First dimension
		y0 -- Second dimension
	
		"""
		self.default_dim = 10, 10
		self.tiles = {}
		if x0 is not None and y0 is not None: # Rectangular Field
			for x in range(0, x0):
				for y in range(0, y0):
					self.tiles[x, y] = Tile(self, x, y)
		elif x0 is not None and y0 is None: # Square Field
			for x in range(0, x0):
				for y in range(0, x0):
					self.tiles[x, y] = Tile(self, x, y)
		else: #Empty Field
			pass
			
	def get_tile(self, x, y, direction = None):
		"""Return the tile or neighbor tile if specified.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction (default None)
	
		"""
		if direction is None:
			return self.tiles.get((x, y))
		else:
			if direction == util.NORTH:
				return self.tiles.get((x, y + 1))
			elif direction == util.EAST:
				return self.tiles.get((x + 1, y))
			elif direction == util.SOUTH:
				return self.tiles.get((x, y - 1))
			elif direction == util.WEST:
				return self.tiles.get((x - 1, y))
			else:
				return None
			
	def add_tile(self, x, y, direction = None):
		"""Add the tile or neighbor tile if specified. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction (default None)
	
		"""
		if direction is None:
			if self.tile_exists(x, y):
				return False
			else:
				self.tiles[x, y] = Tile(self, x, y)
				return True
		else:
			if direction == util.NORTH:
				return self.add_tile(x, y + 1)
			elif direction == util.EAST:
				return self.add_tile(x + 1, y)
			elif direction == util.SOUTH:
				return self.add_tile(x, y - 1)
			elif direction == util.WEST:
				return self.add_tile(x - 1, y)
			else:
				return False
			
	def remove_tile(self, x, y, direction = None):
		"""Remove the tile or neighbor tile if specified. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction (default None)
	
		"""
		if direction is None:
			return self.tiles.pop((x, y), None) is not None
		else:
			if direction == util.NORTH:
				return self.remove_tile(x, y + 1)
			elif direction == util.EAST:
				return self.remove_tile(x + 1, y)
			elif direction == util.SOUTH:
				return self.remove_tile(x, y - 1)
			elif direction == util.WEST:
				return self.remove_tile(x - 1, y)
			else:
				return False

	def tile_exists(self, x, y, direction = None):
		"""Return whether the tile or neighbor tile if specified exists.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction (default None)
	
		"""
		return self.get_tile(x, y, direction) is not None

	def can_travel(self, x, y, direction):
		"""Return whether the tile has a path in the target direction.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction
	
		"""
		if not self.tile_exists(x, y): # Cannot travel if non-existent
			return False
		elif self.tiles[x, y].walls[direction]: # Cannot travel if wall
			return False
		else: # Otherwise, check for neighbor
			return self.tiles[x, y].has_neighbor(direction)

	def remove_wall_to(self, x, y, direction):
		"""Removes the tile's wall in the target direction. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction
	
		"""
		if self.tile_exists(x, y):
			self.tiles[x, y].remove_wall(direction)
			return True
		else:
			return False
	
	def remove_wall_from(self, x, y, direction):
		"""Removes the tile's wall from the target direction. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction
	
		"""
		if self.tile_exists(x, y):
			if self.tile_exists(x, y, direction):
				self.get_tile(x, y, direction).remove_wall(util.get_opposite_direction(direction))
			return True
		else:
			return False

	def remove_wall(self, x, y, direction):
		"""Removes the tile's wall to and from the target direction. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction
		
		"""
		if self.tile_exists(x, y):
			self.remove_wall_to(x, y, direction)
			self.remove_wall_from(x, y, direction)
			return True
		else:
			return False
	
	def add_wall_to(self, x, y, direction):
		"""Adds the tile's wall to the target direction. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction
	
		"""
		if self.tile_exists(x, y):
			self.tiles[x, y].add_wall(direction)
			return True
		else:
			return False
	
	def add_wall_from(self, x, y, direction):
		"""Adds the tile's wall from the target direction. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction
	
		"""
		if self.tile_exists(x, y):
			if self.tile_exists(x, y, direction):
				self.get_tile(x, y, direction).add_wall(util.get_opposite_direction(direction))
			return True
		else:
			return False

	def add_wall(self, x, y, direction):
		"""Adds the tile's wall to and from the target direction. Returns whether the operation was successful.
	
		Arguments:
		x -- First dimension
		y -- Second dimension
		direction -- Neighbor direction
	
		"""
		if self.tile_exists(x, y):
			return self.add_wall_to(x, y, direction) | self.add_wall_from(x, y, direction)
		else:
			return False
			
	def add_person(self, x, y, person):
		""" Adds the given Person to the Tile at (x,y) if possible, and returns
		whether it succeeded. """
		if self.tile_exists(x, y):
			return self.get_tile(x, y).add_person(person)
		else:
			return False
			
	def has_person(self, x, y):
		""" If the Tile at (x,y) exists, returns whether that Tile has a
		Person.  Otherwise, returns None. """
		if self.tile_exists(x, y):
			return self.get_tile(x, y).has_person()		
			
	def remove_person(self, x, y):
		""" If the Tile at (x,y) exists, deletes any Person on it and returns
		whether there was a Person.  Otherwise, returns None. """
		if self.tile_exists(x, y):
			return self.get_tile(x, y).remove_person()
			
	def get_person(self, x, y):
		""" If the Tile at (x,y) exists and has a Person, returns the Person.
		"""
		if self.tile_exists(x,y):
			return self.get_tile(x,y).person;
			
	def get_all_persons(self):
		""" Returns all Persons on the Tiles on this Field. """
		result = []
		for coords in self.tiles:
			if self.has_person(coords[0], coords[1]):
				result.append(self.get_person(coords[0], coords[1]))
		return result

	def get_attacks(self, actor):
		""" Returns all Actions that have attacks that the given Person can
		make. """
		result = []
		for person in self.get_all_persons():
			action = self.find_move_and_attack(actor, person)
			if action != None:
				result.append(action)
		return result
		
	def find_move_and_attack(self, actor, victim):
		""" Returns an Action that the actor can take to attack the victim, if
		such a thing is possible. """
		for move_tile in self.get_moves(actor):
			if self.can_attack_from(move_tile, actor, victim):
				return a.Action(move=move_tile, attack=victim)
		return None
		
	def get_moves(self, actor):
		""" Returns all Tiles that the actor can move to. """
		results = []
		for i in range(actor.movement + 1):
			for j in range(i + 1):
				for x in [-1, 1]:
					for y in [-1, 1]:
						tile = self.get_tile(actor.tile.x + j*x, actor.tile.y + (i-j)*y)
						if tile == None or tile.person != None:
							continue
						if actor.tile.steps_to(tile, actor.max_range) < actor.max_range:
							results.append(tile)
		return list(set(results)) # removes duplicates
		
	def count_healers(self, team):
		""" Counts the number of healers belonging to 'team' left on the Field.
		"""
		count = 0
		for person in self.get_all_persons():
			if person.team == team and isinstance(person, p.Healer):
				count += 1
		return count
		
	def count_hp(self, team):
		""" Counts the total remaining hp of all living Persons belonging to
		the given team. """
		count = 0
		for person in self.get_all_persons():
			if person.team == team:
				count += person.hp
		return count
		
	def count_not_healers(self, team):
		""" Counts the number of Persons who are not healers, who belong to the
		given team and are left on the Field. """
		count = 0
		for person in self.get_all_persons():
			if person.team == team and not isinstance(person, p.Healer):
				count += 1
		return count
		
	def can_attack_from(self, attack_tile, actor, victim):
		""" Returns whether the actor, if located at attack_tile, could attack
		the victim at its current tile. """
		if actor == victim:
			return victim.tile == attack_tile and actor.min_range <= 0
		distance = attack_tile.calculate_manhattan_distance(victim.tile)
		return distance <= actor.max_range and distance >= actor.min_range
		
	def team_destroyed(self):
		""" Returns any team that has no Persons left on the Field, or None if
		both teams have Persons remaining. """
		zero_alive = False
		one_alive = False
		persons = self.get_all_persons()
		for person in persons:
			if person.is_alive():
				if person.team == 0:
					zero_alive = True
				elif person.team == 1:
					one_alive = True
			if zero_alive and one_alive:
				return None
		if not zero_alive:
			return 0
		if not one_alive:
			return 1
			
	def create_save_file(self):
		""" Creates a save file at 'save.txt' and stores the state of the Field
		in it. """
		save = open('save.txt', 'w')
		for tile in self.tiles.values():
			save.write("tile ")
			save.write(str(tile.x) + ' ')
			save.write(str(tile.y) + ' ')
			save.write(str(tile.walls[util.NORTH]) + ' ')
			save.write(str(tile.walls[util.EAST]) + ' ')
			save.write(str(tile.walls[util.SOUTH]) + ' ')
			save.write(str(tile.walls[util.WEST]) + ' ')
			save.write("\n")
		for person in self.get_all_persons():
			save.write("person ")
			save.write(str(person.__class__.__name__) + ' ')
			save.write(str(person.team) + ' ')
			save.write(str(person.tile.x) + ' ')
			save.write(str(person.tile.y) + ' ')
			save.write("\n")
		save.close()
		
	def load_save_file(self, file_name):
		""" Loads the save file at 'save.txt' and changed this Field to match
		it. """
		load = open('save.txt', 'r')
		while 1:
			line = load.readline()
			if not line:
				break
			args = line.split()
			if len(args) != 7:
				pass #raise here
			if args[0] == "tile":
				self.load_tile(args[1:])
			elif args[0] == "person":
				self.load_person(args[1:])
				
	def load_tile(self, args):
		""" Loads a single Tile based on the given args. """
		x, y = int(args[0]), int(args[1])
		self.add_tile(x, y)
		if(args[2] == "True"):
			self.add_wall(x, y, util.NORTH)
		elif(args[2] == "False"):
			pass
		else:
			pass #raise here
		if(args[3] == "True"):
			self.add_wall(x, y, util.EAST)
		elif(args[3] == "False"):
			pass
		else:
			pass #raise here
		if(args[4] == "True"):
			self.add_wall(x, y, util.SOUTH)
		elif(args[4] == "False"):
			pass
		else:
			pass #raise here
		if(args[5] == "True"):
			self.add_wall(x, y, util.WEST)
		elif(args[5] == "False"):
			pass
		else:
			pass #raise here
				
	def load_person(self, args):
		""" Loads a single Person based on the given args. """
		team = int(args[1])
		if args[0] == "Warrior":
			person = p.Warrior(team)
		elif args[0] == "Archer":
			person = p.Archer(team)
		elif args[0] == "Healer":
			person = p.Healer(team)
		else:
			pass #raise here
		x = int(args[2])
		y = int(args[3])
		self.add_person(x, y, person)
		
	def add_rooms(self, size, count = None):
		""" Fills the Field with rooms, separated by walls. """
		if count is None: # if no max rooms specified, fill until full
			count = float("inf")
		rooms = {}
		tiles = set()
		for x in range(self.default_dim[0]): # get all tiles
			for y in range(self.default_dim[1]):
				tiles.add((x, y))
		while len(rooms) < count and len(tiles) > 0: # while we haven't finished adding rooms and there are tiles left
			x, y = random.sample(tiles, 1)[0] # get a tile for the room center
			tiles.remove((x, y))
			if self.tile_exists(x, y): # if already added, skip
				continue
			else: # else add new room at this location
				room = self.add_room(x, y, size)
				rooms[(x, y)] = room
		return rooms
	
	def add_room(self, x, y, size):
		""" Creates a room including the given (x,y) Tile. """
		neighbors = set()
		neighbors.add((x, y))
		tiles = set()
		while len(tiles) < size and len(neighbors) > 0: # while room is smaller than max size and tiles touch the room border
			neighbors_weighted = []
			for nx, ny in neighbors: # for each neighbor in border
				total_n = 0
				for direction in util.directions: # count times it touches a tile in the room
					n = self.get_tile_coords(nx, ny, direction)
					if n in tiles:
						total_n += 1
				total_n = total_n * total_n + 1
				for i in range(total_n): # weight possible next tile to add by times it touches the current room
					neighbors_weighted.append((nx, ny))
			x, y = random.sample(neighbors_weighted, 1)[0] # get one of the tiles
			neighbors.remove((x, y))
			if x not in range(self.default_dim[0]) or y not in range(self.default_dim[1]) or self.tile_exists(x, y): # if the tile is out of range or already exists
				continue # skip
			else: # otherwise
				self.add_tile(x, y) # add it
				tiles.add((x, y))
				for direction in util.directions: # check if it has neighbors not in the current room
					if self.tile_exists(x, y, direction) and self.get_tile_coords(x, y, direction) not in tiles: # add a wall if it does
						self.add_wall(x, y, direction)
					else: # otherwise add it as a border tile
						neighbors.add(self.get_tile_coords(x, y, direction))
		return tiles
		
	def break_walls(self, rooms, walls_to_break):
		""" Breaks the walls in between the rooms. """
		rooms = list(rooms.itervalues())
		room_pairs = []
		for i in range(len(rooms)): # get pairs of rooms
			for j in range(i):
				if i != j:
					room_pairs.append((rooms[i], rooms[j]))
		for room1, room2 in room_pairs: # for each pair of rooms
			edge_tiles = []
			for tile1 in room1:
				for tile2 in room2: # for each pairing of tiles between rooms
					dist = abs(tile1[0] - tile2[0]) + abs(tile1[1] - tile2[1])
					if dist == 1: # get those that border
						edge_tiles.append((tile1, tile2))
			removed_edge_tiles = random.sample(edge_tiles, min(walls_to_break, len(edge_tiles))) # get appropriate number of border tile pairs
			for (r1x, r1y), (r2x, r2y) in removed_edge_tiles: # break the wall between desired pairs
				for direction in util.directions:
					if self.tile_exists(r1x, r1y, direction):
						if self.get_tile(r1x, r1y, direction).x == r2x and self.get_tile(r1x, r1y, direction).y == r2y:
							self.remove_wall(r1x, r1y, direction)
	
	def add_people(self, rooms, count, spacing = 60):
		""" Adds Persons to the Field, using 'spacing' as a recommendation for
		how far apart each team should be. """
		rooms = rooms.items()
		room_pairs = []
		for i in range(len(rooms)): # for each pair of rooms
			for j in range(i):
				if i != j:
					room_pairs.append((rooms[i], rooms[j]))
		valid_rooms = []
		while len(valid_rooms) == 0: # look for a valid room pair
			valid_rooms = []
			for room1, room2 in room_pairs: # for each pair of rooms
				dist = abs(room1[0][0] - room2[0][0]) + abs(room1[0][1] - room2[0][1]) # valid if centers are 'spacing' away
				if len(room1[1]) >= count * 3 and len(room2[1]) >= count * 3 and dist > spacing: # and they can fit all the people for teams
					print "found room pair"
					valid_rooms.append((room1, room2))
			spacing = spacing - 1		
		valid_rooms = random.sample(valid_rooms, 1)[0] # get a valid room pairing
		room_tiles = []
		room_tiles.append(random.sample(valid_rooms[0][1], count * 3))
		room_tiles.append(random.sample(valid_rooms[1][1], count * 3))
		for team in range(2): # add teams to each room
			for i in range(count):
				self.add_person(room_tiles[team][i * 3 + 0][0], room_tiles[team][i * 3 + 0][1], p.Warrior(team))
				self.add_person(room_tiles[team][i * 3 + 1][0], room_tiles[team][i * 3 + 1][1], p.Archer(team))
				self.add_person(room_tiles[team][i * 3 + 2][0], room_tiles[team][i * 3 + 2][1], p.Healer(team))
		print "exit"
				

	def get_tile_coords(self, x, y, direction = None): # gets the x, y coords in a direction from another pair of coords
		""" Returns the coords of the Tile in the given direction from the Tile
		at (x,y). """
		if direction is None:
			return x, y
		else:
			if direction == util.NORTH:
				return x, y + 1
			elif direction == util.EAST:
				return x + 1, y
			elif direction == util.SOUTH:
				return x, y - 1
			elif direction == util.WEST:
				return x - 1, y
			else:
				return None
