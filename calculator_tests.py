import unittest

import field as f
import calculator as c
import person as p

class CalculatorTests(unittest.TestCase):

	def test_next_turn(self):
		field = f.Field(3, 3)
		a = p.Archer(0)
		b = p.Warrior(1)
		field.add_person(0, 0, a)
		field.add_person(1, 1, b)
		calc = c.Calculator(field)
		
		self.assertEqual(calc.get_next_turn(), a)
		self.assertEqual(calc.get_next_turn(), b)
		self.assertEqual(calc.get_next_turn(), a)
		self.assertEqual(calc.get_next_turn(), b)
		self.assertEqual(calc.get_next_turn(), a)
		# archer faster than warrior, so gets to take two turns in a row
		self.assertEqual(calc.get_next_turn(), a)
		
	def test_game(self):
		FIELD_SIZE = FIELD_WIDTH, FIELD_HEIGHT = 15, 15
		field = f.Field(FIELD_WIDTH, FIELD_HEIGHT)

		field.add_person(5, 5, p.Archer(0))
		field.add_person(5, 7, p.Healer(0))
		field.add_person(2, 2, p.Warrior(0))

		field.add_person(14, 14, p.Archer(1))
		field.add_person(14, 12, p.Healer(1))
		field.add_person(10, 10, p.Warrior(1))

		calc = c.Calculator(field)
		x = calc.get_next_turn()
		self.assertNotEqual(x, None)
		
if __name__ == '__main__':
	unittest.main()