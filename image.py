import pygame
import person as p

class ImageLoader(object):

	"""Loads all Person images, turns them into Surfaces, and makes them
		available for use."""
		
	tile_size = (0,0)

	def __init__(self, tile_size, wall_size):
		"""Loads all images and converts them into pygame.Surface objects."""
		# Get all Person subclasses so that we know what the image files
		# are named.
		person_types = p.Person.__subclasses__()
		self.tile_size = (tile_size, tile_size)
		self.hp_size = (tile_size, tile_size / 5)
		self.vert_wall_size = (wall_size, tile_size + 2 * wall_size)
		self.horiz_wall_size = (tile_size + 2 * wall_size, wall_size)
		self.splash_size = (500, 500)
		
		self.teams = [{}, {}]
		self.loop = {}
		for i in [0,1]:
			for role in person_types:
				filename = "images/" + role.__name__ + str(i) + ".png"
				self.teams[i][role.__name__] = self.load(filename)
				self.loop[i, role.__name__] = []
				for j in [0,1]:
					filename = "images/" + role.__name__ + str(i) + str(j) + ".png"
					self.loop[i, role.__name__].append(self.load(filename))

		self.select2 = self.load("images/Select2.png")
		self.red_bar = self.load("images/redBar.png", self.hp_size)
		self.green_bar = self.load("images/greenBar.png", self.hp_size)
		self.teal_wins = self.load("images/tealWins.png", self.splash_size)
		self.purple_wins = self.load("images/purpleWins.png", self.splash_size)
		
		self.grass = self.load('images/grass.png')
		self.vert_wall = self.load('images/wall.png', self.vert_wall_size)
		self.horiz_wall = pygame.transform.rotate(self.vert_wall, -90.0)
		
		self.attack_animation = [self.load('images/attack.png')]
		self.heal_animation = [self.load('images/heal.png')]
		
	def get_special_effect(self, actor):
		""" Returns the animation to be used when the actor attacks or heals.
		"""
		if actor.__class__.__name__ == 'Healer':
			return self.heal_animation
		return self.attack_animation
		
	def get_walking(self, person):
		""" Returns the Persons' walk loop. """
		return self.loop[person.team, person.__class__.__name__]

	def load(self, url, size = (0,0)):
		""" Loads a pygame Surface from the given url with the given
		dimensions.  If no dimensions are given, the Surface will be given
		the same dimensions as the Tiles on the Field. """
		if size == (0,0):
			size = self.tile_size
		return pygame.transform.smoothscale(pygame.image.load(url), size)
	
	def get_image(self, person):
		""" Returns the Surface representation of the given Person."""
		return self.teams[person.team][person.__class__.__name__]

	def get_next_turn(self):
		""" Returns the Surface used to represent the automatic selection of
		the Person who is to move next. """
		return self.select2
		
