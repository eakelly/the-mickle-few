

def calculate_damage(strength, defense):
	"""Returns the amount of damage done by an attacker with the given
		strength to a victim with the given defense."""
	return max(strength * 2 - defense, 0);

class Person(object):

	"""Represents a generic character on a Team.  Can attack and move."""

	max_hp = 10
	hp = 10
	strength = 5
	defense = 5
	speed = 5
	movement = 4 # max steps the person can move at once
	available_movement = 0
	max_range = 0
	min_range = 0
	
	def __init__(self, team):
		"""Creates a Person belonging to the given Team and positioned
			on the given Tile."""
		self.team = team
		self.tile = None
		
	def capable_of_attacking(self, victim):
		"""Returns whether the two given Persons are both alive and somewhere
			on the Field."""
		if victim.hp <= 0 or self.hp <= 0:
			return False
		if self.tile == None or victim.tile == None:
			return False
		return True
		
	def attack(self, victim):
		"""Returns whether self has successfully attacking the victim, possibly
			lowering the victim's hp."""
		if not self.capable_of_attacking(victim):
			return False
		if self.tile.is_neighbor(victim.tile):
			amount_of_damage = calculate_damage(self.strength, victim.defense)
			victim.damage(amount_of_damage)
			#print 'damaged by', amount_of_damage
			return True
		return False
		
	def damage(self, amount_of_damage):
		"""Damages self by the given amount.  Called when self is attacked."""
		self.hp -= amount_of_damage
		if self.hp <= 0:
			self.tile.person = None
			self.tile = None
		if self.hp > self.max_hp:
			self.hp = self.max_hp
		
	def is_alive(self):
		"""Returns whether self has any hp left."""
		return self.hp > 0
		
	def move(self, new_tile):
		"""Tries to move self to the given tile, and returns whether it
			succeeded."""
		if self.tile == None or new_tile == None:
			return False
		can_move = self.tile.can_move(new_tile, self.available_movement)
		if not can_move:
			return False
		self.tile.person.available_movement -= self.tile.steps_to(
													new_tile,
													self.available_movement)
		self.tile.person = None
		self.tile = new_tile
		self.tile.person = self
		return True

class Healer(Person):

	"""A Person capable of healing others."""

	defense = 2
	movement = 4
	max_range = 2
	
	def attack(self, other):
		"""Attempts to heal the given Person if they are within range.  Returns
			success."""
		if self.within_healing_range(other):
			heal_amount = max(10 - other.defense, 1)
			other.damage(-1 * heal_amount)
			return True
		return False
		
	def within_healing_range(self, other):
		"""Returns whether the given Person is within healing range of self."""
		if self.tile == None or other.tile == None:
			return False
		dis = self.tile.calculate_manhattan_distance(other.tile)
		return dis <= self.max_range and dis >= self.min_range
		
class Archer(Person):

	"""A Person that can attack from a distance."""

	strength = 4
	defense = 3
	movement = 3
	max_range = 3
	min_range = 2
	
	def attack(self, victim):
		"""Attempts to attack from a distance.  Returns success."""
		if not self.capable_of_attacking:
			return False
		if self.within_range(victim):
			amount_of_damage = calculate_damage(self.strength, victim.defense)
			victim.damage(amount_of_damage)
			#print 'damaged by', amount_of_damage
			return True
		return False
	
	def within_range(self, other):
		"""Returns whether the given Person is the correct distance away to be
			attacked by self."""
		if self.tile == None or other.tile == None:
			return False
		dis = self.tile.calculate_manhattan_distance(other.tile)
		return dis >= self.min_range and dis <= self.max_range
		return True

class Warrior(Person):

	"""A strong Person."""

	strength = 6
	speed = 4
	movement = 4
	max_range = 1
	min_range = 1
