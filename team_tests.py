import team as t
import person as p

import unittest

class FieldsTests(unittest.TestCase):

	def test_create_team(self):
		team = t.Team([p.Person(0), p.Person(0), p.Person(0)])
		self.assertEqual(len(team.persons), 3)
		
	def test_add_person(self):
		team = t.Team()
		self.assertEqual(len(team.persons), 0)
		
		team.add_person(p.Person(0))
		self.assertEqual(len(team.persons), 1)
		
	def test_alive(self):
		team = t.Team([p.Person(0), p.Person(0), p.Person(0)])
		self.assertEqual(team.count_alive(), 3)
		
		team.persons[0].hp = 0
		self.assertEqual(team.count_alive(), 2)

if __name__ == '__main__':
	unittest.main()
