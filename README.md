A turn-based strategy game.  Characters take turns healing or attacking.  The next character to take a turn is highlighted; right click on the square to move to and/or left click on the character to attack/heal.  Attacking/healing ends the turn.

I (eakelly) apologize for the current state of the movement animation.  You aren't actually going through people/walls; it just looks like you are.

The enemy team is controlled by an AI looking one step into the future to determine the move that will result in the best situation for their team.

Spacebar skips the current character's turn; Esc exits.

Run controller.py to start.

python -m unittest discover --pattern=*_tests.py
to run all tests