
class Team(object):

	"""A group of allied Persons to be controlled by a single player."""

	def __init__(self, persons = []):
		"""Creates the group of Persons."""
		self.persons = persons
	
	def add_person(self, person):
		"""Adds a Person to the group."""
		self.persons.append(person)
		
	def count_alive(self):
		"""Counts the number of Persons still living."""
		return sum(1 for person in self.persons if person.is_alive())