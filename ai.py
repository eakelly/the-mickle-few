
import person as p

import math
from random import shuffle

class Action(object):
	def __init__(self, move = None, attack = None):
		""" Creates an Action holding an optional move and an optional attack.
		This will be used by the AI when determining what a computer-controlled
		Person should do. """
		self.move = move # tile
		self.attack = attack # person

class AI(object):

	def __init__(self, field, team):
		""" Creates an AI controlling the given team on the given Field."""
		self.field = field
		self.team = team
		
	def determine_action(self, actor):
		""" Returns an Action for the actor that is determined to be the best
		at the time of determination.
		
		Arguments:
		actor -- the Person whose turn it currently is on the AI's field.  This
			Person must be on the AI's team.
		"""
		print actor, 'to act from', actor.tile.x, actor.tile.y
		best_score = self.current_score()
		print 'best score:', best_score
		best_action = None
		for action in self.field.get_attacks(actor):
			attackee = action.attack
			score = self.calculate_score(actor, attackee)
			print '    ', score
			if score > best_score:
				best_score = score
				best_action = action
		if best_action != None:
			victim = best_action.attack
			print 'decided to attack', victim, 'at', victim.tile.x, victim.tile.y
			return best_action
		else:
			print 'just move'
			return self.move_towards_enemy(actor)
			
	def move_towards_enemy(self, actor):
		""" To be called when the actor cannot or would rather not attack.
		An enemy of the actor is picked, and the actor moves as close to it as
		possible according to the Manhattan distance. """
		people = self.field.get_all_persons()
		enemy = None
		for person in people:
			if person.team != self.team:
				enemy = person
				break
		moves = self.field.get_moves(actor)
		min_distance = float('inf')
		best_tile = None
		for tile in moves:
			dis = tile.calculate_manhattan_distance(actor.tile)
			if dis < min_distance:
				min_distance = dis
				best_tile = tile
		return Action(move = best_tile)
			
	def current_score(self):
		""" Calculates the utility value of the current state of the Field.
		Higher values are better for the AI's team. """
		self.healer_weight = 200
		self.not_healer_weight = 100
		self.hp_weight = 1		
		return self.calc_team_score(self.team) - self.calc_team_score(1 - self.team)

	def calculate_score(self, actor, actee):
		""" Calculates the utility value of the Field after actor attacks
		actee.  This may be higher or lower than current_score(), depending on
		whether the actor is healing or attacking and whose team the actee is
		on. """
		# damage pos if healing, neg if attacking
		damage = 0
		if isinstance(actor, p.Healer):
			damage = min(max(10 - actee.defense, 1), actee.max_hp - actee.hp)
		else:
			damage = -1 * max(actor.strength * 2 - actee.defense, 0)
		raw_score = self.calc_team_score(self.team) - self.calc_team_score(1 - self.team)
		print 'score now', raw_score
		raw_score += self.change_score(1 if actee.team == self.team else -1, actee, damage)
		print 'score after', raw_score
		return raw_score
		
	def change_score(self, multiplier, actee, damage):
		""" Returns a modifier for the score representing how much damage would
		be done and whether the actee would die from it. """
		current_hp = actee.hp + damage
		score = 0
		if current_hp <= 0:
			score -= self.healer_weight if isinstance(actee, p.Healer) else self.not_healer_weight
		score += self.hp_weight * damage
		return score * multiplier

	def calc_team_score(self, team):
		""" Calculates the utility value of a single team, disregarding how
		well the other team is doing. """
		score = self.healer_weight * self.field.count_healers(team)
		score += self.not_healer_weight * self.field.count_not_healers(team)
		score += self.hp_weight * self.field.count_hp(team)
		return score


		
		
		