import field as f

import unittest

class TileTests(unittest.TestCase):
	
	def testIsNeighbor(self):
		field = f.Field(2, 3)
		tile1 = field.tiles[1,2]
		tile2 = field.tiles[1,1]
		self.assertTrue(tile1.is_neighbor(tile2))
		
	def testNotNeighbor(self):
		field = f.Field(2,3)
		tile1 = field.tiles[1,2]
		tile2 = field.tiles[0,0]
		self.assertFalse(tile1.is_neighbor(tile2))

if __name__ == '__main__':
	unittest.main()