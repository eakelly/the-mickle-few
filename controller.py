import sys
from time import sleep

import ai as a
import calculator as c
import field as f
import image as i
import person as p
import team as t
import view as v

import const
import os.path

"""Starts game and determines effects of players' actions."""


FIELD_SIZE = FIELD_WIDTH, FIELD_HEIGHT = 10, 10

#if os.path.isfile("save.txt"):
if False:
	field = f.Field()
	field.load_save_file("save.txt")
else:
	#field = f.Field(FIELD_WIDTH, FIELD_HEIGHT)
	field = f.Field()
	rooms = field.add_rooms(30)
	field.break_walls(rooms, 1)
	field.add_people(rooms, 2)
	
view = v.View(field, FIELD_SIZE)
calc = c.Calculator(field)
ai = a.AI(field, 0)
ai_action = None
next_turn_person = calc.get_next_turn(view.animation)

while(1):
	loser = field.team_destroyed()
	if loser != None and view.animation == None:
		view.display_game_over(loser)
		break
	else: # game not over
		if(view.view_mode.actions == view.view_mode.default_actions): # find active rectangle
			for rect in view.tile_rects:
				if rect.collidepoint(view.get_tile_anchor(next_turn_person.tile.x, next_turn_person.tile.y)):
					view.active_rect = rect
		# find click or have computer move
		if next_turn_person.team == ai.team and view.animation == None:
			if ai_action == None:
				ai_action = ai.determine_action(next_turn_person)
				
			if ai_action.move != None:
				success = view.view_mode.move(next_turn_person, ai_action.move, next_turn_person.tile)
				if not success:
					print 'MOVE ERROR', next_turn_person, next_turn_person.tile.x, next_turn_person.tile.y
					print '          ', ai_action.move, ai_action.move.x, ai_action.move.y
					sleep(100)
				ai_action.move = None
			elif ai_action.attack != None:
				success = view.view_mode.attack(ai_action.attack.tile, next_turn_person, ai_action.attack)
				if not success:
					print 'ATTACK ERROR'
				ai_action = None
			else:
				view.turn_ended = True
				ai_action = None
		else:
			view.check_for_click() # resets turn_ended to False, changed to True if attack or spacebar
		# find next person to move if turn over
		if view.turn_ended:
			next_turn_person = calc.get_next_turn(view.animation)
			view.turn_ended = False
		view.display()
