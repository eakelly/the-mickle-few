
class TileAnimation(object):
	frame = 0
	start_time = 0
	
	def __init__(self, tile, images, milliseconds = 100): # 10 fps
		""" Creates a non-looping animation that stays on one Tile for the
		duration of its existence. """
		self.tile = tile
		self.images = images
		self.fpms = milliseconds
		
	def display(self, now):
		""" Returns the frame of the animation that should be displayed at the
		given time. """
		if self.start_time == 0:
			self.start_time = now
		elif now >= self.start_time + (self.frame + 1)*self.fpms:
			self.frame += 1
		if self.frame < len(self.images):
			return self.images[self.frame]
		return None
		
class LoopingAnimation(TileAnimation):
	def __init__(self, start, end, end_tile, person, images, total_frames, milliseconds = 100):
		""" Creates a looping animation that moves from one Tile to another,
		then disappears. """
		super(LoopingAnimation, self).__init__(None, images, milliseconds)
		self.x_velocity = (end[0] - start[0]) / (total_frames - 1)
		self.y_velocity = (end[1] - start[1]) / (total_frames - 1)
		self.start = start
		self.end = end
		self.max_frame = total_frames - 1
		self.goal = end_tile
		self.person = person
		
	def display(self, now):
		""" Returns the frame that should be displayed at the given time, as
		well as the (x,y) location it should be displayed at. """
		if self.start_time == 0:
			self.start_time = now
		elif now >= self.start_time + (self.frame + 1)*self.fpms:
			self.frame += 1
		if self.frame > self.max_frame:
			self.goal.person = self.person
			return None, 0, 0
		return self.images[self.frame % len(self.images)], self.get_x(), self.get_y()
		
	def get_x(self):
		""" Returns the x-value at which the animation should be displayed for
		the current frame. """
		return self.get_pos(self.start[0], self.x_velocity)
		
	def get_y(self):
		""" Returns the y-value at which the animation should be displayed for
		the current frame. """
		return self.get_pos(self.start[1], self.y_velocity)
		
	def get_pos(self, x_or_y, velocity):
		""" Returns the x- or y-value at which the animation should be
		displayed for the current frame, given the passed-in velocity. """
		return x_or_y + velocity * self.frame