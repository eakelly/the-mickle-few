import pygame
import sys
import const
import person as p
from collections import defaultdict

import anim as a

DEFAULT = 0
EDIT_MODE = 1

pygame.LEFTMOUSE = 1
pygame.MIDDLEMOUSE = 2
pygame.RIGHTMOUSE = 3
pygame.SCROLLUP = 4
pygame.SCROLLDOWN = 5

class ViewMode(object):
	
	def __init__(self, view):
		self.view = view
		self.default_actions = self.get_default_actions()
		self.edit_actions = self.get_edit_actions()
		self.actions = self.default_actions
		
	def action(self, event):
		if event.type in [pygame.MOUSEBUTTONUP, pygame.MOUSEBUTTONDOWN]:
			self.actions.get((event.type, event.button), (lambda : None))()
		elif event.type in [pygame.KEYUP, pygame.KEYDOWN]:
			self.actions.get((event.type, event.key), (lambda : None))()
		
	def get_default_actions(self):
		default_actions = {}

		default_actions[pygame.MOUSEBUTTONUP, pygame.LEFTMOUSE] = lambda : self.default_left_click()
		default_actions[pygame.MOUSEBUTTONUP, pygame.RIGHTMOUSE] = lambda : self.default_right_click()
		default_actions[pygame.KEYUP, pygame.K_ESCAPE] = lambda : self.default_escape()
		default_actions[pygame.KEYUP, pygame.K_SPACE] = lambda : self.default_space()
		default_actions[pygame.KEYUP, pygame.K_t] = lambda : self.default_t()

		return default_actions
		
	def get_edit_actions(self):
		edit_actions = defaultdict(lambda : (lambda : None))
		
		edit_actions[pygame.MOUSEBUTTONUP, pygame.LEFTMOUSE] = lambda : self.edit_left_click()
		edit_actions[pygame.MOUSEBUTTONUP, pygame.RIGHTMOUSE] = lambda : self.edit_right_click()
		edit_actions[pygame.KEYUP, pygame.K_ESCAPE] = lambda : self.edit_escape()
		edit_actions[pygame.KEYUP, pygame.K_SPACE] = lambda : self.edit_space()
		edit_actions[pygame.KEYUP, pygame.K_w] = lambda : self.edit_w()
		edit_actions[pygame.KEYUP, pygame.K_a] = lambda : self.edit_a()
		edit_actions[pygame.KEYUP, pygame.K_s] = lambda : self.edit_s()
		edit_actions[pygame.KEYUP, pygame.K_d] = lambda : self.edit_d()
		edit_actions[pygame.KEYUP, pygame.K_1] = lambda : self.edit_1()
		edit_actions[pygame.KEYUP, pygame.K_2] = lambda : self.edit_2()
		edit_actions[pygame.KEYUP, pygame.K_3] = lambda : self.edit_3()
		edit_actions[pygame.KEYUP, pygame.K_4] = lambda : self.edit_4()
		edit_actions[pygame.KEYUP, pygame.K_5] = lambda : self.edit_5()
		edit_actions[pygame.KEYUP, pygame.K_6] = lambda : self.edit_6()
		edit_actions[pygame.KEYUP, pygame.K_0] = lambda : self.edit_0()
		edit_actions[pygame.KEYUP, pygame.K_p] = lambda : self.edit_p()
		edit_actions[pygame.KEYUP, pygame.K_t] = lambda : self.edit_t()
		
		return edit_actions
		
	def default_left_click(self):
		rect = self.view.get_mouse_rect()
		if rect not in self.view.field_rects:
			return
		active_rect = self.view.active_rect
		x, y = self.view.get_tile_coords(active_rect[0], active_rect[1])
		if not self.view.field.tile_exists(x, y):
			return
		active_tile = self.view.field.get_tile(x, y)
		active_person = active_tile.person
		target_tile = self.view.get_tile(rect)
		target_person = target_tile.person
		self.attack(target_tile, active_person, target_person)
		
	def attack(self, target_tile, active_person, target_person):
		success = False
		if target_tile.has_person():
			if active_person.attack(target_person):
				images = self.view.image_loader.get_special_effect(active_person)
				self.view.animation = a.TileAnimation(target_tile, images)
				active_person.available_movement = 0
				success = True
				self.view.turn_ended = True
				return True
		return False
	
	def default_right_click(self):
		x, y = self.view.get_tile_coords(self.view.active_rect[0], self.view.active_rect[1])
		active_tile = self.view.field.get_tile(x, y)
		active_person = active_tile.person
		self.view.target_rect = self.view.get_mouse_rect()
		if self.view.target_rect not in self.view.field_rects:
			return
		x, y = self.view.get_tile_coords(self.view.target_rect[0], self.view.target_rect[1])
		if not self.view.field.tile_exists(x, y):
			return
		target_tile = self.view.field.get_tile(x, y)
		self.move(active_person, target_tile, active_tile)
		
	def move(self, active_person, target_tile, active_tile):
		result = active_person.move(target_tile)
		if active_tile != target_tile and result: # animate movement
			target_tile.person = None
			image = self.view.image_loader.get_walking(active_person)
			start = self.view.get_tile_anchor(active_tile.x, active_tile.y)
			end = self.view.get_tile_anchor(target_tile.x, target_tile.y)
			self.view.animation = a.LoopingAnimation(start, end, target_tile, active_person, image, 10, 50)
		return result
		
	def default_escape(self):
		sys.exit()
		
	def default_space(self):
		self.view.turn_ended = True
		
	def default_t(self):
		self.actions = self.edit_actions
		
	def edit_left_click(self):
		self.view.active_rect = self.view.get_mouse_rect()
		self.view.active_tile = self.view.field.get_tile(self.view.active_rect[0], self.view.active_rect[1])
		
	def edit_right_click(self):
		pass
		
	def edit_escape(self):
		sys.exit()
		
	def edit_space(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.remove_wall(x, y, const.NORTH)
			self.view.field.remove_wall(x, y, const.EAST)
			self.view.field.remove_wall(x, y, const.SOUTH)
			self.view.field.remove_wall(x, y, const.WEST)
			self.view.field.remove_tile(x, y)
		else:
			self.view.field.add_tile(x, y)
			
	def edit_w(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			if self.view.field.can_travel(x, y, const.NORTH):
				self.view.field.add_wall(x, y, const.NORTH)
			else:
				self.view.field.remove_wall(x, y, const.NORTH)
		
	def edit_a(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			if self.view.field.can_travel(x, y, const.WEST):
				self.view.field.add_wall(x, y, const.WEST)
			else:
				self.view.field.remove_wall(x, y, const.WEST)
		
	def edit_s(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			if self.view.field.can_travel(x, y, const.SOUTH):
				self.view.field.add_wall(x, y, const.SOUTH)
			else:
				self.view.field.remove_wall(x, y, const.SOUTH)
				
	def edit_d(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			if self.view.field.can_travel(x, y, const.EAST):
				self.view.field.add_wall(x, y, const.EAST)
			else:
				self.view.field.remove_wall(x, y, const.EAST)
				
	def edit_1(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.add_person(x, y, p.Warrior(0))
			
	def edit_2(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.add_person(x, y, p.Archer(0))
			
	def edit_3(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.add_person(x, y, p.Healer(0))
			
	def edit_4(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.add_person(x, y, p.Warrior(1))
			
	def edit_5(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.add_person(x, y, p.Archer(1))
			
	def edit_6(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.add_person(x, y, p.Healer(1))
			
	def edit_0(self):
		rect = self.view.active_rect
		x, y = self.view.get_tile_coords(rect[0], rect[1])
		if self.view.field.tile_exists(x, y):
			self.view.field.remove_person(x, y)
	
	def edit_p(self):
		self.view.field.create_save_file()
		
	def edit_t(self):
		print "toggle to default"
		if self.view.active_rect not in self.view.field_rects or not self.view.get_tile(self.view.active_rect).has_person():
			self.view.turn_ended = True
		self.view.view_mode.actions = self.view.view_mode.default_actions
