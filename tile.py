import const

class Tile(object):
	def __init__(self, field, x, y):
		"""Creates a tile.
	
		Arguments:
		field -- Field that owns the tile.
		x -- First dimension
		y -- Second dimension
	
		"""
		self.field = field
		self.x = x
		self.y = y
		self.walls = [False, False, False, False]
		self.person = None
		
	def has_neighbor(self, direction):
		"""Returns whether the tile has a neighbor in a specific direction.
	
		Arguments:
		direction -- Neighbor direction
	
		"""
		return self.field.tile_exists(self.x, self.y, direction)
	
	def get_neighbor(self, direction):
		"""Returns the tile's neighbor in a specific direction. None if non-existent.
	
		Arguments:
		direction -- Neighbor direction
	
		"""
		return self.field.get_tile(self.x, self.y, direction)

	def add_wall(self, direction):
		"""Adds a wall in the specified direction.
	
		Arguments:
		direction -- Neighbor direction
	
		"""
		if self.walls[direction]:
			return False
		else:
			self.walls[direction] = True
			return True

	def remove_wall(self, direction):
		"""Removes a wall in the specified direction.
	
		Arguments:
		direction -- Neighbor direction
	
		"""
		if self.walls[direction]:
			self.walls[direction] = False
			return True
		else:
			return False
		
	def is_neighbor(self, other):
		"""Returns whether a tile is a neighbor of another tile.
	
		Arguments:
		other -- Other tile
	
		"""
		return self.calculate_manhattan_distance(other) == 1
		
	def calculate_manhattan_distance(self, other):
		"""Returns the manhattan distance from one tile to another.
	
		Arguments:
		other -- Other tile
	
		"""
		return abs(self.x - other.x) + abs(self.y - other.y)
		

	def can_move(self, other, max_steps):
		"""Returns whether a tile can reach another tile within a certain number of moves.

		Arguments:
		other -- Other tile
		max_steps -- Max allowed distance away.

		"""
		if self == other:
			return True
		if self.person == None:
			return False
		if other.person != None:
			return False
		dist = self.steps_to(other, max_steps) 
		if dist <= max_steps:
			#print "Tile", self.x, self.y, " can move to", other.x, other.y
			return True
		else:
			#print "Tile", self.x, self.y, " can't move to", other.x, other.y
			return False
			
	def steps_to(self, other, max_steps):
		"""Returns the distance a tile is from another tile within a certain number of moves.
	
		Arguments:
		other -- Other tile
		max_steps -- Max allowed distance away.
	
		Returns the number of moves away if a path exists, else infinity.
		
		
		"""
		if self == other:
			return 0
		if max_steps <= 0:
			return float("inf")
		curr_dist = float("inf")
		for direction in const.directions:
			if self.has_neighbor(direction) and not self.walls[direction]:
				curr_dist = min(curr_dist, self.get_neighbor(direction).steps_to(other, max_steps - 1))
		return curr_dist + 1

	def add_person(self, person):
		""" Adds a Person to this Tile if this Tile does not currently have an
		associated Person. """
		if self.person is None:
			self.person = person
			self.person.tile = self
			return True
		else:
			return False
			
	def has_person(self):
		""" Returns whether this Tile has an associated Person. """
		return self.person is not None
		
	def remove_person(self):
		""" Removes the associated Person if possible, and returns whether it
		existed. """
		if self.person is None:
			return False
		else:
			self.person = None
			return True
