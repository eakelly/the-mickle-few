NORTH = 0 # increasing y-coord
EAST  = 1
SOUTH = 2
WEST  = 3

directions = [NORTH, EAST, SOUTH, WEST]

def get_opposite_direction(direction):
	""" Returns the cardinal direction opposite of the given direction. """
	if direction == NORTH:
		return SOUTH
	elif direction == EAST:
		return WEST
	elif direction == SOUTH:
		return NORTH
	elif direction == WEST:
		return EAST
